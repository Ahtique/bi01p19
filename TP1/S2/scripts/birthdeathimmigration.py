# Simple Birth and Death Process with Immigration

import numpy as np
import matplotlib.pyplot as plt


np.random.seed(10)
  
def birthdeathmigration(b, d, c, paths):
    X0 = 20 # initial population size
    N = 200 # maximal population size
    
    # Pre-allocation and initialization
    s = np.zeros((paths, N))
    X = np.zeros((paths, N))
    X[:,0] = X0
    s[:,0] = 0.00
    
    for j in range(paths):
        i = 0
        while X[j,i] >= 0 and i < (N-1):
            U1 = np.random.rand()
            U2 = np.random.rand()
            h = - np.log(U1)/((b+d)*X[j,i]+c)
            s[j,i+1] = s[j,i] + h

            if U2 < b*X[j,i]/((b+d)*X[j,i]+c):
                X[j,i+1] = X[j,i] + 1 # a birth occurs
            elif U2 >= b*X[j,i]/((b+d)*X[j,i]+c)\
            and U2 < (b+d)*X[j,i]/((b+d)*X[j,i]+c): 
                X[j,i+1] = X[j,i] - 1 # a death occurs
            elif U2 >= (b+d)*X[j,i]/((b+d)*X[j,i]+c):
                X[j,i+1] = X[j,i] + 1 # immigration occurs
                
            i += 1
            
    return [X, s]


def solve_and_plot(b, d, c, paths):    
    fig, ax = plt.subplots()
    
    [population, sojourn] = birthdeathmigration(b, d, c, paths)
    
    ## Sets axis ranges for plotting
    xmax = max([max(sojourn[k,:]) for k in range(paths)])
        
    ## Generates plots
    for r in range(paths):
        plt.step(sojourn[r,:], population[r,:], 
                 where='pre', label="Path %s" % str(r+1))

    plt.axis([-0.2, xmax+0.2, -2, 32]) #ymax+2])
    ax.set_xlabel('Time', fontsize=14)
    ax.set_ylabel('Population Size', fontsize=14)
    plt.text(0.5, 0.9, '$\\theta = %s$' % c, fontsize=14, 
             horizontalalignment='center',
             verticalalignment='center', 
             transform=ax.transAxes)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.legend(loc=1)
    plt.grid(True)
    plt.savefig("name-of-figre.pdf")
    

#CHANGE THESE:
b = 1.0 # birth rate
d = 0.7 # death rate
c = 0.5 # immigration rate
walkers = 5


solve_and_plot(b, d, c, walkers)
