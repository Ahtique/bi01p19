# -*- coding: utf-8 -*-

#import dendropy
from dendropy.simulate import treesim
import os

#continuous time birth-death process with end when number of descendants is num_extant_tips
t = treesim.birth_death_tree(birth_rate=1.0, death_rate=0.5, num_extant_tips=10)
t.print_plot()

#continuous time birth-death process with end when time = max_time
t2 = treesim.birth_death_tree(birth_rate=0.5, death_rate=0.3, max_time=3)
#save the tree in the newick format
t2.write_to_path(path+"test.newick",schema="newick")

