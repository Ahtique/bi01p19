# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 11:56:14 2019

@author: imaffucci
"""

#!/usr/bin/python
#upload the packages
import numpy as np
import matplotlib.pyplot as plt
#define variables
S = 50 # number of trajectories
T = 150 # simulation time
n = 5 # binomial parameter n
p = 0.22 # binomial parameter p
mu = n*p
Nsurvivals=0 # counts the survival processes
#create a vector [1/mu, 1/mu 2 , 1/mu 3 ...]
Y=np.asarray([1/(mu**x) for x in np.arange(0,T+1)])
for s in range(S):
    Process=[1]
    for t in range(T):
        Z=Process[-1]
        Process.append(np.random.binomial(n*Z,p))
#you add 1 if the process survived at the instant T
    Nsurvivals=Nsurvivals+(Process[-1]>0)
    plt.plot(Process*Y)
plt.title('S ='+ str(S)+' trajectories of Z$_n$/$\mu^n$, mean $\mu$= '+str(np.round(mu,2)))
plt.show()