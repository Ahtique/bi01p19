# -*- coding: utf-8 -*-
#!/usr/bin/python
from matplotlib import pyplot as plt
#initialize the two populations
rabbits=[100]
foxes=[10]
#set the rates
rbr=0.5
rdr=0.015
fbr=0.015
fdr=0.5
#define a time step and the number of cycles
timestep=0.01
cycles=4500
#define the model
for t in range(0,cycles):
    urabbits=rabbits[t]+timestep*(rbr*rabbits[t]-rdr*foxes[t]*rabbits[t])
    ufoxes=foxes[t]+timestep*(-fdr*foxes[t]+fbr*foxes[t]*rabbits[t])
    rabbits.append(urabbits)
    foxes.append(ufoxes)
#plot the populations as a function of time
timepoints=range(cycles+1)
plt.plot(timepoints,foxes,c="red",label="foxes")
plt.plot(timepoints,rabbits,c="blue",label="rabbits")
plt.xlabel("time")
plt.ylabel("population counts")
plt.legend(loc="upper center",frameon=False)
plt.savefig("predator-prey.pdf")