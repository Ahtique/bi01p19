# Simple Birth and Death Process

import numpy as np
import matplotlib.pyplot as plt

#np.random.seed(100)
  

def simplebirthdeath(N, samplepaths, b, d):
    X0 = 50 # initial population size
        
    s = np.zeros((samplepaths, N))
    X = np.zeros((samplepaths, N))
    X[:,0] = X0
    s[:,0] = 0.001
    
    for j in range(samplepaths):
        i = 0
        while X[j,i] > 0 and i < (N-1):
            U1 = np.random.rand()
            U2 = np.random.rand()
            h = - np.log(U1)/((b+d)*X[j,i])
            s[j,i+1] = s[j,i] + h
            
            if U2 < b/(b+d):
                X[j,i+1] = X[j,i] + 1 # birth occurs
            else:
                X[j,i+1] = X[j,i] - 1 # death occurs
                
            i += 1
            
    return [s, X]
    

def solve_and_plot(N, paths, b, d):
    fig, ax = plt.subplots()
    
    [sojourn, population] = simplebirthdeath(N, paths, b, d)
    
    ## Sets axis ranges for plotting
    xmax = max([max(sojourn[k,:]) for k in range(paths)])
    ymax = max([max(population[k,:]) for k in range(paths)])
    
    sojourn[sojourn==0] = np.nan
    
    ## Generates plots
    for r in range(paths):
        plt.step(sojourn[r,:], population[r,:], where='pre', label="Path %s" % str(r+1))
        
    plt.axis([-0.1, xmax+0.2, 0, ymax+2])
    ax.set_xlabel('Time', fontsize=14)
    ax.set_ylabel('Population Size', fontsize=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.legend(loc="upper left")
    plt.savefig("name-of-figure.pdf")

#CHANGE THESE:
maxPop = 2000
walkers = 3  
birth = 1.0
death = 1.0  

    
solve_and_plot(maxPop, walkers, birth, death)
