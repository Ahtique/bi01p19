# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 10:17:40 2019

@author: imaffucci
"""


def myfunction(a,x): #with a as parameter and x as variable
    y=x**a #function definition
    return y 

#to use the function:
Y=myfunction(2,3)
print(Y)

#if you are working with a list of numbers:
L1=[0.10,0.30,0.50]
L2=[2,3,6]
#and you want to sum apply the function to each L1,L2 couple of numbers
#and do something with the obtained results:
results=[]
for n,el in enumerate(L1):
    results.append(myfunction(el,L2[n]))
print(sum(results))