# -*- coding: utf-8 -*-
"""
Created on Mon Feb 18 09:49:47 2019

@author: imaffucci
"""

#!/usr/bin/python
#upload the packages
import numpy as np
import matplotlib.pyplot as plt

#define variables
S = 100 # number of trajectories
T = 80 # simulation time
n = 10 # binomial parameter n
p = 0.105 # binomial parameter p
mu = n*p
Nsurvivals=0 # counts the survival processes
for s in range(S):
    Process=[1]
    for t in range(T):
        Z=Process[-1]
        Process.append(np.random.binomial(n*Z,p))
        #Process.append(np.random.poisson(p,Z))
        Nsurvivals=Nsurvivals+(Process[-1]>0)
    plt.plot(Process,'b')
#calculate mean trajectory
X=np.arange(1,T+1)
Y=[(mu**x) for x in X]
plt.plot(X,Y,'r',label='Expectation')
plt.title('$S=$ '+str(S)+' (Z$_n$) trajectories')
plt.legend(loc='upper left',frameon=False)
plt.show()
