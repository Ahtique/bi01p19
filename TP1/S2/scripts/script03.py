# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 10:17:40 2019

@author: imaffucci
"""

from scipy.optimize import fsolve

#define the function
def eq(x):
    return x**2+ 4*x +4 

#application of fsolve:
#fsolve(function, starting estimate of roots f(x)=0)
solution=fsolve(eq,2)
print(solution)