
import numpy as np
import matplotlib.pyplot as plt
import scipy.misc
import scipy.special
from matplotlib.lines import Line2D
from cycler import cycler


def probdist(N, b):
    t = np.arange(1, N)
    k = np.arange(0, 51) # population vector
    p = np.zeros((len(t), len(k)))
    
    for i in range(len(t)):
        for j in range(len(k)):
            p[i][j] = scipy.special.binom(N+k[j]-1, k[j])\
            *np.exp(-b*N*t[i])\
            *(1-np.exp(-b*t[i]))**k[j]
            
    return [p, t, k]


def solve_and_plot(N, b):
    fig, ax = plt.subplots()
    mycolors = ['cornflowerblue', 'orchid', 'olive', 'gold']
    colors = ax.set_prop_cycle(cycler('color', mycolors))
    marker_style = dict(color=colors, linestyle=':', 
                        marker='o', markersize=6)
    
    [p, t, k] = probdist(N, b)
    
    for y, fill_style in enumerate(Line2D.fillStyles):
        for r in range(len(t)):
            ax.plot(k, p[r][:], fillstyle=fill_style, 
                    **marker_style) 
            
    ax.set_xlabel('Population Size $k$', fontsize=14)
    ax.set_ylabel('Probability $P_k(t)$', fontsize=14)
    
    plt.legend(['$t=%s$' % str(i+1) for i in range(len(t))], fontsize=14, loc=1)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.text(0.5, 0.9, '$\lambda = %s$' % b, fontsize=14, 
         horizontalalignment='center',
         verticalalignment='center', 
         transform=ax.transAxes)
    plt.tight_layout()
    #to plot    
    plt.show()
    #to save figure comment plt.show() and decomment the following
    #plt.savefig("fig.pdf")


N = 5
birth = 0.5

solve_and_plot(N, birth)

