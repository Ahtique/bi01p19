#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 14:06:41 2019

@author: imaffucci
"""

import numpy as np
from matplotlib import pyplot as plt
import os

N = 1000000 #pop tot
S0 = (N - 1)/N #initial susceptible prop
x0 = 1/N #intial infected prop
beta = 0.8 #transmission rate
gamma = 0.2 #recovery rate
t=np.arange(0,101,1) #time range

def C(b,g,x):
    return (b*x)/(b-g-b*x)

def Infec(b,g,t,c):
    return (1-(g/b))*((c*np.exp((b-g)*t))/(1+c*np.exp((b-g)*t)))

const=C(beta,gamma,x0)
It=[]
St=[]
for i in t:
    I=Infec(beta,gamma,i,const) 
    It.append(I)
    St.append(S0-I)
    

#plot the results
plt.plot(t,It, c='red',label="Infected",lw=2)
plt.plot(t,St,c='blue',label="Susceptible",lw=2)
plt.xlim(0,100)
plt.xticks(np.arange(0,110,10))
plt.ylim(-0.1,1.1)
plt.yticks(np.arange(0,1.1,0.1))
plt.legend(loc='center right',frameon=False)
plt.xlabel("Time")
plt.ylabel("Susceptible, Infected")
plt.title("SI model, $\\beta$ = %s, $\\gamma$ = %s, N = %s, S0= %s and x$_0$ = %s"%(beta,gamma,N,S0,x0))
plt.show()
