# -*- coding: utf-8 -*-
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# Total population, N.
N = 1000
# Initial number of infected and recovered individuals, I0 and R0.
I0 = 1
R0 = 0
# Everyone else, S0, is susceptible to infection initially.
S0 = N - I0 - R0
# transmission rate, beta, and mean recovery rate, gamma, (in 1/days).
beta =  0.2
gamma = 0.1 
# A grid of time points (in days)
t = np.linspace(0, 160, 160)

# The SIR model differential equations.
def deriv(y, t, N, beta, gamma):
    S, I, R = y
    dSdt = -beta * S * I / N
    dIdt = beta * S * I / N - gamma * I
    dRdt = gamma * I
    return dSdt, dIdt, dRdt

# Initial conditions vector
y0 = S0, I0, R0
# Integrate the SIR equations over the time grid, t.
ret = odeint(deriv, y0, t, args=(N, beta, gamma))
S, I, R = ret.T

# Plot the data on three separate curves for S(t), I(t) and R(t)
plt.plot(t, S/N, 'b', alpha=0.5, lw=2, label='Susceptible')
plt.plot(t, I/N, 'r', alpha=0.5, lw=2, label='Infected')
plt.plot(t, R/N, 'g', alpha=0.5, lw=2, label='Recovered with immunity')
plt.xlabel('Time /days')
plt.ylabel('Susceptible, Infected, Recovered')
plt.ylim(-0.1,1.2)
plt.xlim(min(t),max(t))
plt.xticks(np.arange(min(t),max(t)+10,20))
plt.yticks(np.arange(0,1.1,0.1))
plt.legend(loc="upper right", frameon=False)
plt.title("SIR model, N = %s, S0 = %s, x0 = %s, $\\beta$ = %s, $\\gamma$ = %s"%(N,S0,I0,beta,gamma))
plt.show()
