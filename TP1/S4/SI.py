# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import os


#define variables
N = 1000000 #pop tot
S0 = (N - 1)/N #initial susceptible prop
x0 = 1/N #intial infected prop
beta = 0.6 #transmission rate
t=np.arange(0,101,1) #time range    

#function definition of cumulative growth curve as a function of time
def infected(a,b,x):
    y = (a*np.exp(b*x))/(1-a+a*np.exp(b*x))
    return y



#computed the number of infected and susceptible as a function of time
It=[]
St=[]
for i in t:
    I=infected(x0,beta,i) 
    It.append(I)
    St.append(S0-I)

#plot the results
plt.plot(t,It, c='red',label="Infected",lw=2)
plt.plot(t,St,c='blue',label="Susceptible",lw=2)
plt.xlim(0,100)
plt.xticks(np.arange(0,110,10))
plt.ylim(-0.1,1.1)
plt.yticks(np.arange(0,1.1,0.1))
plt.legend(loc='center right',frameon=False)
plt.xlabel("Time")
plt.ylabel("Susceptible, Infected")
plt.title("SI model, $\\beta$ = %s, N = %s, S0 = %s and x$_0$ = %s"%(beta,N,S0,x0))
#plt.show()
plt.savefig("SI.pdf")

