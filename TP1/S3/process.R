a= c(0.2,0.2,0.2,0.2,0.2)

#define the function to calculate the survivals for each generations
branch <- function(n) {
  z <- c(1,rep(0,n))
  for (i in 2:(n+1)) {
    z[i] <- sum( sample(0:2, z[i-1], replace=T, prob=a))
  }
  return(z) }

process <- branch(10)

pdf("rendus/mybranching.pdf")
#to plot the resulting branching process 
plot(process,type='b',xlab="generation",ylab=expression("Z"[n]))
dev.off()