
#define the function to calculate the survivals for each generations
branch_geom <- function(n, p) {
  z <- c(1,rep(0,n))
  for (i in 2:(n+1)) {
    z[i] <- sum(rgeom(z[i-1],p))
  }
  return(z)
}
generation <- 10
replicates <- 5000
p <- 0.25
simlist <-replicate(replicates,branch_geom(generation,p)[generation+1])
simlist <- simlist == 0
sum(simlist)/replicates

