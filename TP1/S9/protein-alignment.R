#load the library
library(Biostrings)
library(seqinr)
# choose the score matrix
data(BLOSUM62)
BLOSUM62 #to display the matrix
#define the sequences
p1 <- "AGVSILNYA"
p2 <- "VSILYAKR"
#perform the alignment allowing to obtain both the alignment and the score
# using the BLOSUM62 matrix, a go = -10 and a ge = -1
globalAlignp1p2 <- pairwiseAlignment(p1,p2, substitutionMatrix = "BLOSUM62",gapOpening = -10, gapExtension = -1, scoreOnly = FALSE)
# print the alignment summary
globalAlignp1p2