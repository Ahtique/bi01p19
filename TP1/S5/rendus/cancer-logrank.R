#log-rank test: U = (O-E)^2/V, O-E = summ of(MiB - Mi*riB/Ri) for all i between 1 and n, V=summ of (riB/Ri*riA/Ri)

#INITIAL DATA
A<-c(9,13,15,17,34,40,45,48)
B<-c(1,4,5,10,12,18,23,27,35)
#censoring (1=no censoring, 0=censoring)
cens.A<-c(1,1,1,1,1,1,1,0)
cens.B<-c(1,1,1,1,1,1,1,1,1)
#treating data (ex-equo)
dfA<-data.frame(A,cens.A)
dfA.cens<-subset(dfA,cens.A==0)
dfA.uncens<-subset(dfA,cens.A==1)
DiA.cens<-list()
Acens<-list()
for(i in unique(dfA.cens$A)){
  dia<-lapply(sum(dfA.cens$A==i),as.numeric)
  DiA.cens<-c(DiA.cens,dia)
}
Acens<-dfA.cens$A[!duplicated(dfA.cens$A)]

Aunc<-list()
DiA.uncens<-list()
for(i in unique(dfA.uncens$A)){
  dia<-lapply(sum(dfA.uncens$A==i),as.numeric)
  DiA.uncens<-c(DiA.uncens,dia)
}
Aunc<-dfA.uncens$A[!duplicated(dfA.uncens$A)]
cens.A<-list()
for(i in Aunc){
  cens.A<-c(cens.A,1)
}
for(i in Acens){
  cens.A<-c(cens.A,0)
}
cens.A<-as.vector(unlist(cens.A))
Anew<-append(Aunc,Acens)
DiA<-c(DiA.uncens,DiA.cens)
DiA<-as.vector(unlist(DiA))
GiA<-(rep(0,length(Anew)))
rm(Aunc,dfA,DiA.cens,DiA.uncens,dfA.cens,dfA.uncens,dia)

dfB<-data.frame(B,cens.B)
dfB.cens<-subset(dfB,cens.B==0)
dfB.uncens<-subset(dfB,cens.B==1)
DiB.cens<-list()
Bcens<-list()
for(i in unique(dfB.cens$B)){
  dib<-lapply(sum(dfB.cens$B==i),as.numeric)
  DiB.cens<-c(DiB.cens,dib)
}
Bcens<-dfB.cens$B[!duplicated(dfB.cens$B)]

Bunc<-list()
DiB.uncens<-list()
for(i in unique(dfB.uncens$B)){
  dib<-lapply(sum(dfB.uncens$B==i),as.numeric)
  DiB.uncens<-c(DiB.uncens,dib)
}
Bunc<-dfB.uncens$B[!duplicated(dfB.uncens$B)]

cens.B<-list()
for(i in Bunc){
  cens.B<-c(cens.B,1)
}
for(i in Bcens){
  cens.B<-c(cens.B,0)
}
cens.B<-as.vector(unlist(cens.B))
Bnew<-append(Bunc,Bcens)
DiB<-c(DiB.uncens,DiB.cens)
DiB<-as.vector(unlist(DiB))
GiB<-(rep(1,length(Bnew)))
rm(Bunc,dfB,DiB.cens,DiB.uncens,dfB.cens,dfB.uncens,dib)

#JOINING A and B
Ti<-c(Anew,Bnew)
Ci<-c(cens.A,cens.B)
Gi<-c(GiA,GiB)
Di<-c(DiA,DiB)
DiA<-list()
for(i in 1:length(Ti)){
  if(Gi[i]==0){
    DiA<-c(DiA,Di[i])
  }
  else{
    DiA<-c(DiA,0)
  }
  }
DiA<-as.vector(unlist(DiA))
DiB<-list()
for(i in 1:length(Ti)){
  if(Gi[i]==1){
    DiB<-c(DiB,Di[i])
  }
  else{
    DiB<-c(DiB,0)
  }
}
DiB<-as.vector(unlist(DiB))
df <-data.frame(Ti,Ci,Gi,Di,DiA,DiB)
df <- df[order(df$Ti),]
#counting the subject at risk of group A including TiB
totA=0
for(i in 1:length(df$Ti)){
  if(df$Gi[i]==0){
    totA<-totA+as.numeric(df$Di[i])
  }}
RiA<-list()
for(i in 1:length(df$Ti)){
  if(df$Gi[i]==0){
    if(i==1){
      riA=totA
      RiA<-c(RiA,riA)
    }
    if(i>1){
      if(df$Ti[i-1]==df$Ti[i] & df$Ci[i-1] == 1){
        riA<-totA - sum(as.numeric(df$DiA[1:i])) + as.numeric(df$DiA[i])
        RiA<-c(RiA,riA)
      }
      else if(df$Ti[i-1]==df$Ti[i] & df$Ci[i-1] == 0){
        riA<-totA - (i) +1 + sum(df$Gi[1:i]==1)
        RiA<-c(RiA,riA)
      }
      else{
        riA=totA - (i) +1 + sum(df$Gi[1:i]==1)
        RiA<-c(RiA,riA)
      }
    }
  }
  else if(df$Gi[i]==1){
    if(i==1){
      riA<-totA
      RiA<-c(RiA,riA)
    }
    else{
      if(df$Gi[i-1]==0){
        if(df$Ci[i-1]==1){
          riA<-as.numeric(RiA[i-1])-as.numeric(df$DiA[i-1])
          RiA<-c(RiA,riA)}
        else if(df$Ci[i-1]==0){
          riA<-as.numeric(RiA[i-1])-as.numeric(df$DiA[i-1])
          RiA<-c(RiA,riA)}
        }
      if(df$Gi[i-1]==1){
        riA<-RiA[i-1]
        RiA<-c(RiA,riA)}
    }
  }
}
df$RiA=RiA
#counting the subject at risk of group B including TiA
totB=0
for(i in 1:length(df$Ti)){
  if(df$Gi[i]==1){
    totB<-totB+as.numeric(df$Di[i])
}}
RiB<-list()
for(i in 1:length(df$Ti)){
  if(df$Gi[i]==1){
    if(i==1){
      riB=totB
      RiB<-c(RiB,riB)
    }
    if(i>1){
      riB=totB - sum(as.numeric(df$DiB[1:i])) + as.numeric(df$DiB[i])
      RiB<-c(RiB,riB)
      }
  }
  else if(df$Gi[i]==0){
    if(i==1){
      riB<-totB
      RiB<-c(RiB,riB)
    }
    else{
      if(df$Gi[i-1]==1){
        if(df$Ci[i-1]==1){
          riB<-as.numeric(RiB[i-1])-as.numeric(df$DiB[i-1])
          RiB<-c(RiB,riB)}
        else if(df$Ci[i-1]==0){
          riB<-as.numeric(RiB[i-1])-as.numeric(df$DiB[i-1])
          RiB<-c(RiB,riB)}}
      if(df$Gi[i-1]==0){
        riB<-RiB[i-1]
        RiB<-c(RiB,riB)}
    }
  }
}
df$RiB=RiB
#removing the censored data
df.unc<-subset(df,Ci==1)
#calculating the total number of subjects at risk at Ti
Ri<-list()
for(i in 1:length(df.unc$Ti)){
  ri<-as.numeric(df.unc$RiA[i])+as.numeric(df.unc$RiB[i])
  Ri<-c(Ri,ri)
}
df.unc$Ri=Ri
#counting the total number of subjects experiencing the event at time Ti
Mi<-list()
for(i in 1:length(df.unc$Ti)){
  mi<-as.numeric(df.unc$DiA[i])+as.numeric(df.unc$DiB[i])
  Mi<-c(Mi,mi)
}
#appending data to table
df.unc$Mi=Mi
#ratio between RiB and Ri
Rb.Ri<-list()
for(i in 1:length(df.unc$Ti)){
  div<-as.numeric(df.unc$RiB[i])/as.numeric(df.unc$Ri[i])
  Rb.Ri<-c(Rb.Ri,div)}
df.unc$RbRi<-Rb.Ri
#computing W
W<-0
for(i in 1:length(df.unc$Ti)){
  w<-as.numeric(df.unc$DiB[i]) - as.numeric(df.unc$Mi[i])*as.numeric(df.unc$RbRi[i])
  W<-W+as.numeric(w)
}
print(paste0("W = ", W))
#computing variance
Var<-0
for(i in 1:length(df.unc$Ti)){
  v<-(as.numeric(df.unc$RiB[i])/as.numeric(df.unc$Ri[i]))*(as.numeric(df.unc$RiA[i])/as.numeric(df.unc$Ri[i]))
  Var<-Var+as.numeric(v)
}
print(paste0("Var = ", Var))
#computing U ~ chi square
U<-W**2/Var
print(paste0("U = ", U))
#calculate the p-value
print(paste0("p val : ", pchisq(U, df=1,lower.tail=FALSE)))
