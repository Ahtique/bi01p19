#load the library
library(survival)
#create the data
#Freireich A
# A<-c(6,6,6,6,7,9,10,10,11,13,16,17,19,20,22,23,25,32,32,34,35)
# cens.A<-c(1,1,1,0,1,0,1,0,0,1,1,0,0,0,1,1,0,0,0,0,0)
#Freireich B
A<-c(1,1,2,2,2,3,4,4,5,5,8,8,8,8,11,11,12,15,17,22,23)
cens.A<-c(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)
df<-data.frame(A,cens.A)
#compute the KM S(t)
SF <-survfit(Surv(A,cens.A)~1,data=df,type="fleming-harrington")
SFs <- summary(SF)
print(SFs)
#plot the results
pdf("rendus/FreireichB-HF-survival.pdf")
plot(SF,xlab="time",ylab="S(t)",col="green")
dev.off()