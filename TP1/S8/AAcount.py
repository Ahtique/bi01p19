#!/usr/bin/python

import numpy as np

SEQ1 = "Q8R4I4.fasta"
SEQ2 = "Q9UNN4.fasta"

s1 = np.loadtxt(SEQ1, skiprows=1, dtype=str)
s2 = np.loadtxt(SEQ2, skiprows=1, dtype=str)

# separe residues
seq1 = []
for el in s1:
    l = list(el)
    for i in l:
        seq1.append(i)
seq2 = []
for el in s2:
    l = list(el)
    for i in l:
        seq2.append(i)

# count length
print("Length of L1 : " + str(len(seq1)))
print("Length of L2 : " + str(len(seq2)))

# count occurrences for each sequence
res1 = {}
for keys in seq1:
    res1[keys] = res1.get(keys, 0) + 1
print("The composition of SEQ1 is: \n" + str(res1))

res2 = {}
for keys in seq2:
    res2[keys] = res2.get(keys, 0) + 1
print("The composition of SEQ2 is: \n" + str(res2))

# count the number of threonines
print("In SEQ1 there are %s threonines" % res1["T"])
print("In SEQ2 there are %s threonines" % res2["T"])

# compute frequences
perc1 = float(res1["T"]) / float(len(seq1)) * 100
print("In SEQ1 there is the %.2f%%  of threonines" % (perc1))
perc2 = float(res2["T"]) / float(len(seq2)) * 100
print("In SEQ2 there is the %.2f%%  of threonines" % (perc2))

# ==============================================
# number of prolines
print("In SEQ1 there are %s prolines" % res1["P"])
print("In SEQ2 there are %s prolines" % res2["P"])

# compute frequences
perc1 = float(res1["P"]) / float(len(seq1)) * 100
print("In SEQ1 there is the %.2f%%  of prolines" % (perc1))
perc2 = float(res2["P"]) / float(len(seq2)) * 100
print("In SEQ2 there is the %.2f%%  of prolines" % (perc2))

# ==============================================
# number of valines
print("In SEQ1 there are %s valines" % res1["V"])
print("In SEQ2 there are %s valines" % res2["V"])

# compute frequences
perc1 = float(res1["V"]) / float(len(seq1)) * 100
print("In SEQ1 there is the %.2f%%  of valines" % (perc1))
perc2 = float(res2["V"]) / float(len(seq2)) * 100
print("In SEQ2 there is the %.2f%%  of valines" % (perc2))