#!/usr/bin/python

import numpy as np
from matplotlib import pyplot as plt
from skimage.filters import sobel
from skimage.restoration import denoise_tv_chambolle


def delta(x, y):
    return 0 if x == y else 1


def M(seq1, seq2, i, j, k):
    return sum(delta(x, y) for x, y in zip(seq1[i:i + k], seq2[j:j + k]))


def makeMatrix(seq1, seq2, k):
    n = len(seq1)
    m = len(seq2)
    return [[M(seq1, seq2, i, j, k) for j in np.arange(m - k + 1)] for i in
            np.arange(n - k + 1)]

SEQ1 = "Q8R4I4.fasta"
SEQ2 = "Q9UNN4.fasta"

s1 = np.loadtxt(SEQ1, skiprows=1, dtype=str)
print(s1)
s2 = np.loadtxt(SEQ2, skiprows=1, dtype=str)
print(s2)

seq1 = []
for el in s1:
    l = list(el)
    for i in l:
        seq1.append(i)
seq2 = []
for el in s2:
    l = list(el)
    for i in l:
        seq2.append(i)
mat = np.array(makeMatrix(seq1, seq2, 1))
mat = sobel(mat)
plt.figure(figsize=(10, 10))
dotplot = plt.imshow(mat, cmap="gray",
                     interpolation="none")
xt = plt.xticks(np.arange(0, len(seq1) + 1, 20))
yt = plt.yticks(np.arange(0, len(seq2) + 1, 20))
plt.xlabel("Q8R4I4")
plt.ylabel("Q9UNN4")
plt.show()
# plt.savefig("My-dotplot-python.pdf")
